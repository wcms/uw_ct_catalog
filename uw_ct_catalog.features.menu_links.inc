<?php

/**
 * @file
 * uw_ct_catalog.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_catalog_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_catalog-settings:admin/config/system/uw_ct_catalog_settings.
  $menu_links['menu-site-management_catalog-settings:admin/config/system/uw_ct_catalog_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_ct_catalog_settings',
    'router_path' => 'admin/config/system',
    'link_title' => 'Catalog settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_catalog-settings:admin/config/system/uw_ct_catalog_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_catalogs-categories:admin/structure/taxonomy/uw_catalog_item_categories.
  $menu_links['menu-site-manager-vocabularies_catalogs-categories:admin/structure/taxonomy/uw_catalog_item_categories'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_catalog_item_categories',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Catalogs: categories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_catalogs-categories:admin/structure/taxonomy/uw_catalog_item_categories',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_catalogs:admin/structure/taxonomy/uw_catalog_catalogs.
  $menu_links['menu-site-manager-vocabularies_catalogs:admin/structure/taxonomy/uw_catalog_catalogs'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_catalog_catalogs',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Catalogs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_catalogs:admin/structure/taxonomy/uw_catalog_catalogs',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Catalog settings');
  t('Catalogs');
  t('Catalogs: categories');

  return $menu_links;
}
