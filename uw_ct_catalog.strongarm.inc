<?php

/**
 * @file
 * uw_ct_catalog.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_ct_catalog_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_uw_catalog_item';
  $strongarm->value = '0';
  $export['comment_anonymous_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_uw_catalog_item';
  $strongarm->value = 1;
  $export['comment_default_mode_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_uw_catalog_item';
  $strongarm->value = '50';
  $export['comment_default_per_page_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_uw_catalog_item';
  $strongarm->value = 0;
  $export['comment_form_location_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_uw_catalog_item';
  $strongarm->value = '0';
  $export['comment_preview_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_uw_catalog_item';
  $strongarm->value = 0;
  $export['comment_subject_field_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_uw_catalog_item';
  $strongarm->value = '1';
  $export['comment_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'default_menu_link_enabled_uw_catalog_item';
  $strongarm->value = 0;
  $export['default_menu_link_enabled_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_uw_catalog_item';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_uw_catalog_item';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_uw_catalog_item';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_uw_catalog_item';
  $strongarm->value = 1;
  $export['enable_revisions_page_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__uw_catalog_item';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'entity_teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'ical' => array(
        'custom_settings' => TRUE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'embedded' => array(
        'custom_settings' => TRUE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '13',
        ),
        'redirect' => array(
          'weight' => '15',
        ),
        'xmlsitemap' => array(
          'weight' => '12',
        ),
        'locations' => array(
          'weight' => '16',
        ),
        'language' => array(
          'weight' => '18',
        ),
        'metatags' => array(
          'weight' => '17',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_taxonomy_term__uw_catalog_catalogs';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => TRUE,
      ),
      'ical' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'description' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'full' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_taxonomy_term__uw_catalog_catalogs'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_uw_catalog_item';
  $strongarm->value = '4';
  $export['language_content_type_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'linkchecker_scan_comment_uw_catalog_item';
  $strongarm->value = 0;
  $export['linkchecker_scan_comment_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'linkchecker_scan_node_uw_catalog_item';
  $strongarm->value = 1;
  $export['linkchecker_scan_node_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_cancel_uw_catalog_item';
  $strongarm->value = '2';
  $export['mb_content_cancel_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_sac_uw_catalog_item';
  $strongarm->value = '0';
  $export['mb_content_sac_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_tabcn_uw_catalog_item';
  $strongarm->value = 0;
  $export['mb_content_tabcn_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_uw_catalog_item';
  $strongarm->value = array();
  $export['menu_options_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_uw_catalog_item';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_uw_catalog_item';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_uw_ct_catalog';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_uw_ct_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_uw_catalog_item';
  $strongarm->value = '0';
  $export['node_preview_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_revision_delete_number_uw_catalog_item';
  $strongarm->value = '50';
  $export['node_revision_delete_number_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_revision_delete_track_uw_catalog_item';
  $strongarm->value = 1;
  $export['node_revision_delete_track_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_uw_catalog_item';
  $strongarm->value = 0;
  $export['node_submitted_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_title_type_uw_catalog_item';
  $strongarm->value = '';
  $export['page_title_type_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_uw_catalog_item_en_pattern';
  $strongarm->value = '';
  $export['pathauto_node_uw_catalog_item_en_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_uw_catalog_item_pattern';
  $strongarm->value = 'catalogs/[node:field_catalog_catalogs]/[node:title]';
  $export['pathauto_node_uw_catalog_item_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_uw_catalog_item_und_pattern';
  $strongarm->value = '';
  $export['pathauto_node_uw_catalog_item_und_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_uwaterloo_faculties_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_uwaterloo_faculties_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_uw_catalog_catalogs_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_uw_catalog_catalogs_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_uw_catalog_item_categories_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_uw_catalog_item_categories_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_enable_uw_catalog_item';
  $strongarm->value = 1;
  $export['scheduler_publish_enable_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_revision_uw_catalog_item';
  $strongarm->value = 1;
  $export['scheduler_publish_revision_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_touch_uw_catalog_item';
  $strongarm->value = 1;
  $export['scheduler_publish_touch_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_enable_uw_catalog_item';
  $strongarm->value = 1;
  $export['scheduler_unpublish_enable_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_revision_uw_catalog_item';
  $strongarm->value = 1;
  $export['scheduler_unpublish_revision_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'searcher_config';
  $strongarm->value = array(
    'forms' => array(
      'toggle_forms' => 0,
      'move_keyword_search' => 0,
      'advanced_populate' => 0,
      'remove_containing_wrapper' => 'default',
      'advanced_expand' => 'default',
    ),
    'fields' => array(
      'containing_any' => array(
        'remove' => 0,
        'roles' => array(
          1 => 0,
          2 => 0,
          16 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          17 => 0,
        ),
      ),
      'containing_phrase' => array(
        'remove' => 0,
        'roles' => array(
          1 => 0,
          2 => 0,
          16 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          17 => 0,
        ),
      ),
      'containing_none' => array(
        'remove' => 0,
        'roles' => array(
          1 => 0,
          2 => 0,
          16 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          17 => 0,
        ),
      ),
      'types' => array(
        'remove' => 0,
        'roles' => array(
          1 => 0,
          2 => 0,
          16 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          17 => 0,
        ),
        'filter' => array(
          'uw_site_footer' => 0,
          'uw_web_page' => 0,
        ),
        'groupings' => array(),
      ),
      'category' => array(
        'remove' => 0,
        'roles' => array(
          0 => 0,
          1 => 0,
          2 => 0,
          16 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          17 => 0,
        ),
      ),
    ),
    'restrictions' => array(
      'admin_bypass' => 1,
    ),
  );
  $export['searcher_config'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'tvi_vocab_uw_catalog_catalogs';
  $strongarm->value = array(
    'is_default' => FALSE,
    'type' => 'vocab',
    'xid' => 'uw_catalog_catalogs',
    'status' => 1,
    'view_name' => 'uw_catalog_glossary',
    'display' => 'catalog_glue',
    'pass_arguments' => 1,
  );
  $export['tvi_vocab_uw_catalog_catalogs'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_ct_catalog_search_enable';
  $strongarm->value = '1';
  $export['uw_ct_catalog_search_enable'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_ct_catalog_wide_enable';
  $strongarm->value = '1';
  $export['uw_ct_catalog_wide_enable'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_uw_catalog_item';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_uw_catalog_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_node_uw_catalog_item';
  $strongarm->value = array(
    'status' => '1',
    'priority' => '0.5',
  );
  $export['xmlsitemap_settings_node_uw_catalog_item'] = $strongarm;

  return $export;
}
