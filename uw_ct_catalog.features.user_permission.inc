<?php

/**
 * @file
 * uw_ct_catalog.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_catalog_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer catalog search'.
  $permissions['administer catalog search'] = array(
    'name' => 'administer catalog search',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_ct_catalog',
  );

  // Exported permission: 'create field_catalog_popularity'.
  $permissions['create field_catalog_popularity'] = array(
    'name' => 'create field_catalog_popularity',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create uw_catalog_item content'.
  $permissions['create uw_catalog_item content'] = array(
    'name' => 'create uw_catalog_item content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'define view for terms in uw_catalog_catalogs'.
  $permissions['define view for terms in uw_catalog_catalogs'] = array(
    'name' => 'define view for terms in uw_catalog_catalogs',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary uw_catalog_catalogs'.
  $permissions['define view for vocabulary uw_catalog_catalogs'] = array(
    'name' => 'define view for vocabulary uw_catalog_catalogs',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'delete any uw_catalog_item content'.
  $permissions['delete any uw_catalog_item content'] = array(
    'name' => 'delete any uw_catalog_item content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_catalog_item content'.
  $permissions['delete own uw_catalog_item content'] = array(
    'name' => 'delete own uw_catalog_item content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uw_catalog_catalogs'.
  $permissions['delete terms in uw_catalog_catalogs'] = array(
    'name' => 'delete terms in uw_catalog_catalogs',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_catalog_item content'.
  $permissions['edit any uw_catalog_item content'] = array(
    'name' => 'edit any uw_catalog_item content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit field_catalog_popularity'.
  $permissions['edit field_catalog_popularity'] = array(
    'name' => 'edit field_catalog_popularity',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_catalog_popularity'.
  $permissions['edit own field_catalog_popularity'] = array(
    'name' => 'edit own field_catalog_popularity',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own uw_catalog_item content'.
  $permissions['edit own uw_catalog_item content'] = array(
    'name' => 'edit own uw_catalog_item content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uw_catalog_catalogs'.
  $permissions['edit terms in uw_catalog_catalogs'] = array(
    'name' => 'edit terms in uw_catalog_catalogs',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_catalog_item revision log entry'.
  $permissions['enter uw_catalog_item revision log entry'] = array(
    'name' => 'enter uw_catalog_item revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_catalog_item published option'.
  $permissions['override uw_catalog_item published option'] = array(
    'name' => 'override uw_catalog_item published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_catalog_item revision option'.
  $permissions['override uw_catalog_item revision option'] = array(
    'name' => 'override uw_catalog_item revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_catalog_item content'.
  $permissions['search uw_catalog_item content'] = array(
    'name' => 'search uw_catalog_item content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'view field_catalog_popularity'.
  $permissions['view field_catalog_popularity'] = array(
    'name' => 'view field_catalog_popularity',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_catalog_popularity'.
  $permissions['view own field_catalog_popularity'] = array(
    'name' => 'view own field_catalog_popularity',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
