<?php

/**
 * @file
 * uw_ct_catalog.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_catalog_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_catalog_audience_category_faculty';
  $context->description = 'Displays tabs and search box on the sidebar for single audience, category and faculty view pages';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'catalogs/*/audience/*' => 'catalogs/*/audience/*',
        'catalogs/*/category/*' => 'catalogs/*/category/*',
        'catalogs/*/faculty/*' => 'catalogs/*/faculty/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_catalog-catalog_tablike_sidebar' => array(
          'module' => 'uw_ct_catalog',
          'delta' => 'catalog_tablike_sidebar',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
        'views-2c4e6479e15a1326bbe0f3c52e36b8f8' => array(
          'module' => 'views',
          'delta' => '2c4e6479e15a1326bbe0f3c52e36b8f8',
          'region' => 'sidebar_second',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays tabs and search box on the sidebar for single audience, category and faculty view pages');
  $export['uw_catalog_audience_category_faculty'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_catalog_item';
  $context->description = 'Displays search for catalog';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'catalogs' => 'catalogs',
        'catalogs/*/new' => 'catalogs/*/new',
        'catalogs/*/popular' => 'catalogs/*/popular',
        'catalogs/*/faculty' => 'catalogs/*/faculty',
        'catalogs/*/audience' => 'catalogs/*/audience',
        'catalogs/*/category' => 'catalogs/*/category',
        'taxonomy/term/*' => 'taxonomy/term/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-2c4e6479e15a1326bbe0f3c52e36b8f8' => array(
          'module' => 'views',
          'delta' => '2c4e6479e15a1326bbe0f3c52e36b8f8',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays search for catalog');
  $export['uw_catalog_item'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_catalog_item_search_home_sidebar';
  $context->description = 'Displays catalog search block on the sidebar of homepage.';
  $context->tag = 'Content';
  $context->conditions = array(
    'context_var' => array(
      'values' => array(
        'uw_ct_catalog_search_on_homepage|1' => 'uw_ct_catalog_search_on_homepage|1',
      ),
    ),
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-2c4e6479e15a1326bbe0f3c52e36b8f8' => array(
          'module' => 'views',
          'delta' => '2c4e6479e15a1326bbe0f3c52e36b8f8',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays catalog search block on the sidebar of homepage.');
  $export['uw_catalog_item_search_home_sidebar'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_catalog_item_sidebar_content';
  $context->description = 'Displays catalog page sidebar content.';
  $context->tag = 'Content';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'uw_catalog_item' => 'uw_catalog_item',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_catalog-uw_sidebar_block_catalog' => array(
          'module' => 'uw_ct_catalog',
          'delta' => 'uw_sidebar_block_catalog',
          'region' => 'sidebar_second',
          'weight' => '-20',
        ),
        'uw_ct_catalog-uw_sidebar_related_links_catalog' => array(
          'module' => 'uw_ct_catalog',
          'delta' => 'uw_sidebar_related_links_catalog',
          'region' => 'sidebar_second',
          'weight' => '-19',
        ),
        'uw_ct_catalog-catalog_tablike_sidebar' => array(
          'module' => 'uw_ct_catalog',
          'delta' => 'catalog_tablike_sidebar',
          'region' => 'sidebar_second',
          'weight' => '-18',
        ),
        'views-2c4e6479e15a1326bbe0f3c52e36b8f8' => array(
          'module' => 'views',
          'delta' => '2c4e6479e15a1326bbe0f3c52e36b8f8',
          'region' => 'sidebar_second',
          'weight' => '-17',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays catalog page sidebar content.');
  $export['uw_catalog_item_sidebar_content'] = $context;

  return $export;
}
