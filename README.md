# Catalog Content type

## Setting up a catalog content type

Enable the module, either by going to Modules and searching for Catalog or by
using the command line (in a terminal window) to run `drush en uw_ct_catalog`.

Once the module is enabled go to My Workbench -> Create Manage/Content ->
Catalog item

* First, you will need to go to **Manage catalogs** to create a new **Catalog**
* Once you have a catalog created you can then create a **Catalog item** and add
  it to the **Catalog** you created.
* If you want to be able to search for Catalog items from the sites Homepage you
  will need to enable that feature via **Edit catalog settings**
