/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * This js file allows to control to show/hide Popularity, Category, Faculty and
 * Audience field based on selected catalog.
 */

(function ($) {
  Drupal.behaviors.uw_ct_catalog = {
    attach: function (context) {
      $('#edit-field-catalog-catalogs-und input').click(function () {
        // Check if the radio is checked.
        if ($(this).is(":checked")) {
          // Retrieve the value.
          var val = $(this).val();
          // Get all tabs info from drupal variable (function uw_ct_catalog_form_uw_catalog_item_node_form_alter())
          var tabs = (Drupal.settings.uw_ct_catalog.tabs);
          // Get the number of catalog terms.
          var tabsLength = tabs.length / 6;
          // The flag to stop loop.
          var flag = 0;
          // Loop all catalog terms, when found, then stop looping.
          for (var i = 0; i < tabsLength; i++) {
           if (flag == 0) {
              var j = i * 6;
              /* The tabs data is like: 0,0,3,4,5,55,0,0,0,0,5,57
               * The six is as a group, in each group,
               * The first one is for Tab New which we don't care.
               * The second one is for Tab Popular.
               * The third one is for Tab Category.
               * The fourth one is for Tab Faculty.
               * The fifth one is for Tab Audience.
               */
              if (val == tabs[5 + j]) {
                if (tabs[1 + j] == 0) {
                  $('#edit-field-catalog-popularity').hide();
                }
                if (tabs[2 + j] == 0) {
                  $('#edit-field-catalog-item-category').hide();
                }
                if (tabs[3 + j] == 0) {
                  $('#edit-field-catalog-audience').hide();
                }
                if (tabs[4 + j] == 0) {
                  $('#edit-field-catalog-faculty').hide();
                }
                // When found the given val, then set flag to 1, will stop looping.
                flag = 1;
              }
              // Show all four fields when the selected value is not the given tid or Not selected.
              else {
                $('#edit-field-catalog-popularity').show();
                $('#edit-field-catalog-item-category').show();
                $('#edit-field-catalog-faculty').show();
                $('#edit-field-catalog-audience').show();
              }
            }
          }
        }
      });
      // Fire a click event if the field is checked on page load so the fields hide.
      $('#edit-field-catalog-catalogs-und input:checked').click();
    }
  }
})(jQuery);
